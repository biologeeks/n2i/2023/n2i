import os
import random

from flask import Flask, g, request, session, jsonify, url_for
from flask.templating import render_template
from flask_babel import Babel, gettext, ngettext
from flask_caching import Cache
from pytz import timezone
from dotenv import load_dotenv, dotenv_values


import sparql

config = dotenv_values(".env")

BABEL_DEFAULT_LOCALE = 'en'
BABEL_DEFAULT_TIMEZONE = 'UTC'
BABEL_TRANSLATION_DIRECTORIES = 'translations'

config = {
    **dotenv_values(".env"),
    **dotenv_values(".env.shared"),   
    **dotenv_values(".env.secret"),
    **os.environ,
}


LANGUAGES = {
    'en': 'English',
    'es': 'Español',
    'fr': 'Français',
    'km': 'Shikomori',
    'fa': 'فارسی',
    'sn': 'Wolof',
}



def get_locale():
    user = getattr(g, 'user', None)
    if "language" in session:
        return session["language"]
    if user is not None:
        return user.locale
    return request.accept_languages.best_match(LANGUAGES)

def get_timezone():
    user = getattr(g, 'user', None)
    if user is not None:
        return user.timezone

app = Flask(__name__)
babel = Babel(app, locale_selector=get_locale, timezone_selector=get_timezone)
app.config.from_mapping(config)
cache = Cache(app)

@app.context_processor
def inject_dict_for_all_templates():
    return dict(languages=LANGUAGES)



@app.route('/lang/<language>')
def set_language(language=None):
    if language not in LANGUAGES:
        language = 'en'
        return jsonify(success=False)
    else:
        session["language"] = language
        return jsonify(success=True)

@app.route("/")
def index():
    return render_template("index.html")

# Sample quiz questions and answers
questions = [
    {"question": "q1", "options": ["a", "b", "c", "d"], "correct_answer": "b"},
    {"question": "q2", "options": ["a", "b", "c", "d"], "correct_answer": "d"},
    # Add more questions
]

@cache.memoize()
def get_quote():
    return sparql.quote()


@app.route('/quizz', methods=['GET', 'POST'])
def quizz():
    if request.method == 'POST':
        user_answers = {key: request.form.get(key) for key in request.form}
        score = sum(user_answers.get(q["question"]) == q["correct_answer"] for q in questions)
        return render_template('quizz.html', questions=questions, submitted=True, score=score, user_answers=user_answers, total=len(questions))
    else:
        return render_template('quizz.html', questions=questions, submitted=False)
    
@app.route('/api/quote/', methods=['GET', 'POST'])
def quote():
    quotes = get_quote()
    quote = random.choice(quotes)
    return jsonify(quote)

@cache.memoize()
def get_searcher_climate_change():
    return sparql.searcher_climate_change()

@app.route('/api/searcher/')
def searcher():
    data = get_searcher_climate_change()
    return jsonify(data)


if __name__ == '__main__':
    app.run(debug=True)
