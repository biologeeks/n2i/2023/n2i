#!/usr/bin/env bash
pybabel extract -F babel.cfg -o messages.pot .
pybabel extract -F babel.cfg -k lazy_gettext -o messages.pot .
pybabel compile -d translations
pybabel update -i messages.pot -d translations
