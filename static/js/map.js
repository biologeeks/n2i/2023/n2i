var map = L.map('map').setView([51.505, -0.09], 2);

L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    minZoom: 1,

    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);

// var corner1 = L.latLng(-15, 10),
// corner2 = L.latLng(10, -10),
// bounds = L.latLngBounds(corner1, corner2);

// map.setMaxBounds([
//     [40.712, -74.227],
//     [40.774, -74.125]
// ]);


// Retrieve the data
fetch("/api/searcher/")
    .then(response => response.json())
    .then(data => {
        // Create a marker for each data point
        data.forEach(function (item) {
            let {geoloc} = item;
            let {lat, lon} = parseGeoloc(geoloc.value);
            let {academicsLabel} = item;
            addMarker(lat, lon, academicsLabel.value);
        });
    });

function addMarker(lat, lon, name) {
    let marker = L.circle([lat, lon]).addTo(map)
    let popup = name == "Valérie Masson-Delmotte" ? '<b>Valérie Masson Delmotte: <a href="https://youtu.be/wQ3GIehTHoQ">Conférence de l\'Espace des Sciences de Rennes</a></b>' : '<b>' + name + '</b>';
    marker.bindPopup(popup);
}

function parseGeoloc(position) {
    let coordinates = position.replace("Point(", "").replace(")", "").split(" ");
    return {
        lon: coordinates[0],
        lat: coordinates[1]
    };
}