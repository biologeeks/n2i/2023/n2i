let languageSelectorButton = document.getElementById('language-selector-button');
let languageSelector = document.getElementById('language-selector');
languageSelectorButton.addEventListener('click', function () {
    fetch('/lang/' + languageSelector.value, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(function (message) {
        if (message.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + message.status);
            return;
        }
        else {
            location.reload();
        }
    });
});