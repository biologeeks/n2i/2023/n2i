let quoteElement = document.getElementById("quote");
let quoteButton = document.getElementById("next-quote-button");
let quoteAnchor = document.getElementById("quote-source");

quoteButton.addEventListener("click", function() {
    getQuote();
});

function updateQuote(quote) {
    quoteElement.innerText = quote.itemLabel.value;
    quoteAnchor.href = quote.item.value;
}

function getQuote() {
    let endpoint = "api/quote";
    fetch(endpoint)
        .then(function(response) {
            return response.json();
        })
        .then(function(quote) {
            updateQuote(quote);
        })
        .catch(function(error) {
            return "An error occurred";
        });
}

window.onload = getQuote();