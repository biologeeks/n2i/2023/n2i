import sys
from SPARQLWrapper import SPARQLWrapper, JSON

endpoint_url = "https://query.wikidata.org/sparql"

def is_quote(quote):
    """Dumb filter"""
    string = quote["itemLabel"]["value"]
    return " " in string

def quote():
    query = """SELECT ?item ?itemLabel WHERE {
    ?item wdt:P31 wd:Q35102.
    SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
    }"""
    results = get_results(endpoint_url, query)
    quotes = results["results"]["bindings"]
    quotes = filter(is_quote, quotes)
    return list(quotes)


def get_results(endpoint_url, query):
    user_agent = "WDQS-example Python/%s.%s" % (sys.version_info[0], sys.version_info[1])
    # TODO adjust user agent; see https://w.wiki/CX6
    sparql = SPARQLWrapper(endpoint_url, agent=user_agent)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    return sparql.query().convert()


def searcher_climate_change():
    query = """
    SELECT DISTINCT ?institution ?institutionLabel ?academics ?academicsLabel ?degree ?degreeLabel ?geoloc ?image ?field_of_work ?field_of_workLabel WHERE {
  ?academics wdt:P31 wd:Q5;
    p:P69 ?statement.
  OPTIONAL { ?academics wdt:P18 ?image. }
  ?statement ps:P69 ?institution.
  ?institution wdt:P625 ?geoloc.
  ?academics (wdt:P101/(wdt:P279*)) wd:Q52139.
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
  OPTIONAL { ?academics wdt:P101 ?field_of_work. }
}"""
    results = get_results(endpoint_url, query)
    return results["results"]["bindings"]