import os
import sys

sys.path.append(os.path.dirname(__file__))
from app import app as application
application.debug = True
