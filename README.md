# N2I 2023

## Setup

_Quick start_

```bash
python3 -m venv .venv/n2i
source .venv/n2i/bin/activate
pip install -r requirements.txt
```

_.env_

Create a `.env` file with at least:

```text
SECRET_KEY="a very secret key"
```

You could also add
```text
FLASK_ENV=production
CACHE_TYPE=FileSystemCache
CACHE_DIR=cache
```

## Syncing to alwaysdata

On alwaysdata, python3 is version 3.12.0 (could be changed if needed)

```bash
rsync -avzu --exclude-from=.rsyncignore ./ biologeeks@ssh-biologeeks.alwaysdata.net:~/n2i/
```

## i18n and l10n 
_internationalization and localization_

To change some translations, go to `./translations/` find the folder for your language and update the values accordingly.

Before and then, you should probably run:
```bash
bash translate.sh
```
to update the files from the html/js entries.

To add a localized string in `jinja/html`, type `{{ _('the text') }}`.
In Flask app python scripts it is rather with `gettext`.

We translated the website in several languages.


## a11y 
_accessibility_

## Wikidata

We used SPARQL to query Wikidata ontologies for two queries:
- Climatologue researchers around the world
- Quotes referenced by Wikidata

## Team Members

- Abdou Diouf
- Cristina Marianini
- Kader Benamad Houssein
- Mahsa Razavi
- Naïa Périnelle
- Océane Saïbou
- Samuel Ortion

## Easter Eggs

<details>
<summary>Click to see the easter eggs</summary>

### Valérie Masson Delmotte

The first easter egg is an extern link for a conference of Valérie Masson Delmotte, a French paleoclimatologist, member of the IPCC.
A little hint to find the easter egg is to look at where she do its researches. (You are allowed to use your SPARQL-fu to find this :wink:)

</details>