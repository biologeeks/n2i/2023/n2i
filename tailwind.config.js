/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: 'jit',
  content: ["./templates/**/*.html"],
  theme: {
    extend: {
      colors: {
        'biolo-greens': '#105114',
        'biolo-greens-light': '#1d6f1d',
        'ipcc-blue': '#1f77b4',
        'ipcc-blue-light': '#aec7e8',
      }
    },
  },
  plugins: [],
  darkMode: 'class',
}

