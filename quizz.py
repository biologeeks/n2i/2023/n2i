from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)

# Sample quiz data (replace this with your actual quiz data)
quiz_data = [
    {
        'question': 'q1',
        'options': ['a', 'b', 'c', 'd'],
        'correct_answer': 'b'
    },
    {
        'question': 'q2',
        'options': ['a', 'b', 'c', 'd'],
        'correct_answer': 'd'
    },
    # Add more questions as needed
]

@app.route('/')
def index():
    return render_template('quizz.html', quiz_data=quiz_data)

@app.route('/submit', methods=['POST'])
def submit():
    score = 0
    user_answers = request.form.to_dict()

    # Calculate the score based on user answers
    for question in quiz_data:
        if user_answers.get(question['question']) == question['correct_answer']:
            score += 1

    return f'Your score is: {score}/{len(quiz_data)}'

if __name__ == '__main__':
    app.run(debug=True)
